string = "Some text with numbers like 4,5 and 6 floats like 4.5 and 6.7 and mixed like Python3"

def find_sum_of_numeric_values(string):
    result = 0
    for i in range(len(string)):
        char = string[i]
        if char.isnumeric():
            result += int(char)
        else:
            continue

    return result

print(find_sum_of_numeric_values(string))
