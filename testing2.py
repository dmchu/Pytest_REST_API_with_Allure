# given a list of items, return a unified string if item is a string
a_list = [1, "H", 3, "ello"]
# result = "Hello"

def combine_unified_string_out_of_string_values(a_list):
    result = []
    for char in a_list:
        if isinstance(char, str):
            result.append(char)
        else:
            continue

    return "".join(result)

print(combine_unified_string_out_of_string_values(a_list))