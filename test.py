# 3. given the list of integers and a standalone integer, find all pairs which sum into standalone one:

a_list = [1, 2, 3, 4, 5, 6, 7]
desired_num = 6
# result = (1, 5), (2, 4)


# def find_sum(a_list, desired_num):
#     nums_visited = {}
#     results = []
#     for i in range(len(a_list)):
#         num = a_list[i]
#         add_num = desired_num - num
#         if add_num in nums_visited:
#             results.append((a_list[i], a_list[nums_visited[add_num]]))
#         else:
#             nums_visited[num] = i
#     return results

def find_sum_optimized(a_list, desired_num):
    nums_visited = set()
    results = []
    for num in a_list:
        add_num = desired_num - num
        if add_num in nums_visited:
            results.append((num, add_num))
        nums_visited.add(num)
    return results
# print(find_sum(a_list, desired_num))
print(find_sum_optimized(a_list, desired_num))