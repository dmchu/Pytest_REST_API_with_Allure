# 104. Maximum Depth of Binary Tree
class Solution:
# Recursive solution
    def __init__(self):
        self.max_level = 1
    def dive(self, node, level):
        if not node:
            return
        self.max_level = max(self.max_level, level)
        self.dive(node.left, level + 1)
        self.dive(node.right, level + 1)

    def maxDepth(self, root: Optional[TreeNode]) -> int:
        if not root:
            return 0
        self.dive(root, 1)
        return self.max_level
# Time complexity: O(n), where n is the number of nodes in the binary tree.
# Space complexity: O(h), where h is the height of the binary tree.
# ==========================================================================
# Iterative solution
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        if not root:
            return 0
        max_level = 0
        stack = [[root, 1]]
        while stack:
            current_node, current_level = stack.pop()
            max_level = max(current_level, max_level)
            if current_node.left:
                stack.append([current_node.left, current_level + 1])
            if current_node.right:
                stack.append([current_node.right, current_level + 1])
        return max_level
# Time complexity: O(n), where n is the number of nodes in the binary tree.
# Space complexity: O(n)

