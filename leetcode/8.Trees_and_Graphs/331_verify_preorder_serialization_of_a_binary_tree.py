# 331. Verify Preorder Serialization of a Binary Tree
def isValidSerialization(preorder: str) -> bool:
    # O(N) solution
    slots = 1
    chars = preorder.split(',')
    for char in chars:
        slots -= 1
        if slots < 0:
            return False
        elif char != "#":
            slots += 2
    return slots == 0
# Time complexity: O(N) - linear time complexity relative
# Space complexity: O(N) - linear space complexity relative
# to the length of the preorder string.
def isValidSerialization2(preorder: str) -> bool:
    # O(1) solution
    slots = 1
    for i in range(len(preorder)):
        char = preorder[i]
        if char == ',':
            slots -= 1
            if slots < 0:
                return False
            prev_char = preorder[i - 1]
            if prev_char != "#":
                slots += 2
    if preorder[-1] == "#":
        slots -= 1
    else:
        slots += 1
    return slots == 0
# Time complexity: O(N) - linear time complexity relative
# to the length of the preorder string.
# Space complexity: O(1) - constant space complexity.