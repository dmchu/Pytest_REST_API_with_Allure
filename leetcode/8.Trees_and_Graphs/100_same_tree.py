# 100. Same Tree
class Solution:
    def __init__(self):
        self.same_tree = True

    def helper(self, node_a, node_b):
        if (not node_a and not node_b) or not self.same_tree:
            return
        if not node_a or not node_b or node_a.val != node_b.val:
            self.same_tree = False
            return
        self.helper(node_a.left, node_b.left)
        self.helper(node_a.right, node_b.right)

    def isSameTree(self, p: Optional[TreeNode], q: Optional[TreeNode]) -> bool:
        self.helper(p, q)
        return self.same_tree
# Time complexity: O(n), O(n) - linear time complexity relative to the number
# of nodes in the smaller of the two binary trees (p or q).
# Space complexity: O(h) - space complexity depends on the height of the
# smaller of the two binary trees (p or q).
