# 207. Course Schedule
class Solution:
    def __init__(self):
        self.adjacency_list = {}
        self.visited = {}

    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:

        for vertex in range(numCourses):
            self.adjacency_list[vertex] = []
            self.visited[vertex] = "unvisited"
        for edge in prerequisites:
            v1, v2 = edge
            self.adjacency_list[v1].append(v2)
        for vertex in range(numCourses):
            if self.visited[vertex] == "unvisited":
                if not self.dfs(vertex):
                    return False
        return True

    def dfs(self, vertex):
        self.visited[vertex] = "visiting"
        for neighbor_vertex in self.adjacency_list[vertex]:
            if self.visited[neighbor_vertex] == "visiting":
                return False
            if self.visited[neighbor_vertex] == "unvisited" and not self.dfs(neighbor_vertex):
                return False
        self.visited[vertex] = "visited"
        return True
# Time complexity:O(V+E) considering V vertices and E edges.
# Space complexity: O(V+E) considering V vertices and E edges.
