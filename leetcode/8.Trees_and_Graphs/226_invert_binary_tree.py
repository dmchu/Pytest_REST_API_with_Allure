# 226. Invert Binary Tree
class Solution:
    def invertTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        self.swap_nodes(root)
        return root

    def swap_nodes(self, node):
        if not node:
            return
        node.left, node.right = node.right, node.left
        self.swap_nodes(node.left)
        self.swap_nodes(node.right)

# Time complexity: O(n), where n is the number of nodes in the binary tree.
# Space complexity: O(h), where h is the height of the binary tree.
# In the worst case, O(n) due to recursion stack space.

