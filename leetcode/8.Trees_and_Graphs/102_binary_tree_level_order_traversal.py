# 102. Binary Tree Level Order Traversal
class Solution:
    def __init__(self):
        self.res = []

    def dive(self, node, level):
        if not node:
            return

        if len(self.res) <= level:
            self.res.append([])

        self.res[level].append(node.val)
        self.dive(node.left, level + 1)
        self.dive(node.right, level + 1)

    def levelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        self.dive(root, 0)
        return self.res
# Time complexity: O(n), where n is the number of nodes in the binary tree.
# Space complexity: O(n)
