# 230. Kth Smallest Element in a BST
class Solution:
    def __init__(self):
        self.bst_list = []

    def explore_bst(self, node):
        if node.left:
            self.explore_bst(node.left)
        self.bst_list.append(node.val)
        if node.right:
            self.explore_bst(node.right)

    def kthSmallest(self, root: Optional[TreeNode], k: int) -> int:
        self.explore_bst(root)
        return self.bst_list[k - 1]
# Time complexity: O(n log n) due to the sorting step
# after collecting node values.
# Space complexity: O(n) due to the space used by self.bst_list
# to store all node values
