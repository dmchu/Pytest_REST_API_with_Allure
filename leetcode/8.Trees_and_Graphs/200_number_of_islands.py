# 200. Number of Islands
class Solution:
    def __init__(self):
        self.number_of_islands = 0

    def numIslands(self, grid: List[List[str]]) -> int:
        for row in range(len(grid)):
            for col in range(len(grid[0])):
                if grid[row][col] == "1":
                    self.number_of_islands += 1
                    self.dive_the_land(grid, row, col)
        return self.number_of_islands

    def dive_the_land(self, grid, row, col):
        if row < 0 or row >= len(grid) or col < 0 or col >= len(grid[0]) or grid[row][col] == "0":
            return
        grid[row][col] = "0"
        self.dive_the_land(grid, row + 1, col)
        self.dive_the_land(grid, row - 1, col)
        self.dive_the_land(grid, row, col + 1)
        self.dive_the_land(grid, row, col - 1)
# Time complexity: O(m * n) where m is the number of rows and n is the number of columns.
# Space complexity: O(m * n) due to the recursive calls in the worst case.
