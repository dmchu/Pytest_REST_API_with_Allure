# 572. Subtree of Another Tree
class Solution:
    def same_tree(self, p, q):
        if p and q:
            return  p.val == q.val and self.same_tree(p.left, q.left) and self.same_tree(p.right, q.right)
        return p == q

    def isSubtree(self, root: Optional[TreeNode], subRoot: Optional[TreeNode]) -> bool:
        stack = [root]
        while stack:
            current = stack.pop()
            if current.left:
                stack.append(current.left)
            if current.right:
                stack.append(current.right)
            if self.same_tree(current, subRoot):
                return True
        return False
# Time complexity: O(n*m) where is the number of nodes in root and m is the number of nodes in subRoot.
# Space complexity: O(n)
