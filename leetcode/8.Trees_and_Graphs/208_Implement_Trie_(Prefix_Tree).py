# 208. Implement Trie (Prefix Tree)
class TrieNode:
    def __init__(self):
        self.is_end_of_word = False
        self.children = {}
class Trie:
    def __init__(self):
        self.root = TrieNode()
    def insert(self, word: str) -> None:
        node = self.root
        for char in word:
            if char not in node.children:
                node.children[char] = TrieNode()
            node = node.children[char]
        node.is_end_of_word = True
    def search(self, word: str) -> bool:
        node = self.root
        for char in word:
            if char not in node.children:
                return False
            node = node.children[char]
        return node.is_end_of_word
    def startsWith(self, prefix: str) -> bool:
        node = self.root
        for char in prefix:
            if char not in node.children:
                return False
            node = node.children[char]
        return True
# Insert Operation Time complexity: O(m) where m is the length of the word being inserted.
# Search Operation Time complexity: O(m) where m is the length of the word being searched.
# Starts With Operation Time complexity: O(p) where p is the length of the prefix being checked.
# Space Complexity: O(N) where N is the total number of characters inserted into the trie.