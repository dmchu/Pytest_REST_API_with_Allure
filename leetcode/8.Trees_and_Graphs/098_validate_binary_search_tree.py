# 98. Validate Binary Search Tree
class Solution:
    def __init__(self):
        self.validity = True

    def evaluate_bst(self,node, minimum, maximum):
        if not node or not self.validity:
            return
        if ((minimum != None and node.val <= minimum) or
                (maximum != None and node.val >= maximum)):
            self.validity = False
            return
        self.evaluate_bst(node.left, minimum, node.val)
        self.evaluate_bst(node.right, node.val, maximum)

    def isValidBST(self, root: Optional[TreeNode]) -> bool:
        self.evaluate_bst(root, None, None)
        return self.validity
# Time complexity: O(n) - linear time complexity relative to the
# number of nodes in the binary tree.
# Space complexity: O(h) due to recursion stack space, where
# h is the height of the binary tree.
