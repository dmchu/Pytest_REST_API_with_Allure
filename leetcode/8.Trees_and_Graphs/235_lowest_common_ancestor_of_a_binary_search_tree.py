# 235. Lowest Common Ancestor of a Binary Search Tree
def lowestCommonAncestor(root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
    current = root
    while current:
        if current.val < p.val and current.val < q.val:
            current = current.right
        elif current.val > p.val and current.val > q.val:
            current = current.left
        else:
            return current
# Time complexity: O(h), where h is the height of the BST.
# This is optimal for finding the LCA in a BST.
# Space complexity: O(1), constant space usage, which is efficient
