# 133. Clone Graph
from typing import Optional
class Solution:
    def cloneGraph(self, node: Optional['Node']) -> Optional['Node']:
        if not node:
            return node

        visited = {}
        node_clone = Node(node.val, [])
        visited[node] = node_clone

        stack = [node]

        while stack:
            popped = stack.pop()
            for neighbor in popped.neighbors:
                if neighbor not in visited:
                    neighbor_clone = Node(neighbor.val, [])
                    visited[neighbor] = neighbor_clone
                    stack.append(neighbor)
                visited[popped].neighbors.append(visited[neighbor])

        return node_clone
# Time Complexity:
# O(V+E) where V is the number of nodes and E is the number of edges in the graph.
# Space Complexity:
# O(V+E) considering the space used for the cloned graph, mapping
# (visited dictionary), and auxiliary space like the stack.
