# 347. Top K Frequent Elements
from heapq import heappop, heappush

def topKFrequent(self, nums: list[int], k: int) -> list[int]:
    element_count = {}
    for num in nums:
        if num not in element_count:
            element_count[num] = 1
        else:
            element_count[num] += 1

    list_of_nums = []
    for key, value in element_count.items():
        heappush(list_of_nums, [value, key])
        if len(list_of_nums) > k:
            heappop(list_of_nums)
    result = []

    while list_of_nums:
        result.append(heappop(list_of_nums)[1])

    return result
# Time Complexity: O(n log k), where n is the number of elements
# in nums and k is the value of k.
# Space Complexity: O(n+k), where n is the space used by the
# dictionary element_count and k is the space used by the min-heap list_of_nums.
