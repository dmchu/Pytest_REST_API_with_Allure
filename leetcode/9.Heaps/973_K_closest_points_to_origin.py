# 973. K Closest Points to Origin
from heapq import heappop, heappush

def kClosest(points: list[list[int]], k: int) -> list[list[int]]:
    list_of_coordinates = []
    for point in points:
        x, y = point
        distance = -(x**2 + y**2)
        coordinates = [distance, point]
        heappush(list_of_coordinates, coordinates)
        if len(list_of_coordinates) > k:
            heappop(list_of_coordinates)
    result = []
    while list_of_coordinates:
        result.append(heappop(list_of_coordinates)[1])
    return result

# Time Complexity: O(n log k), where n is the number of points
# in points and k is the value of k.
# Space Complexity: O(k), where k is the size of the heap used
# to store the closest points to the origin.