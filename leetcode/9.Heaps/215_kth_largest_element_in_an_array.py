# 215. Kth Largest Element in an Array
from heapq import heappush, heappop

def findKthLargest(nums: list[int], k: int) -> int:
    result = []
    for num in nums:
        heappush(result, num)
        if len(result) > k:
            heappop(result)
    return result[0]

# Time Complexity: O(n log k), where n is the number of elements
# in nums and k is the value of k.
# Space Complexity: O(k), where k is the size of the heap
# used to store the top k largest elements encountered so far.
