# 23. Merge k Sorted Lists
from heapq import heappush, heappop

def mergeKLists(self, lists: list[Optional[ListNode]]) -> Optional[ListNode]:
    queue = []
    dummy = ListNode(None)

    for i in range(len(lists)):
        head_node = lists[i]
        if head_node:
            heappush(queue, [head_node.val, i, head_node])

    sorted_list_tail = dummy

    while queue:
        current = heappop(queue)
        node = current[2]
        sorted_list_tail.next = node
        if node.next:
            heappush(queue, [node.next.val, current[1], node.next])
        sorted_list_tail = sorted_list_tail.next

    return dummy.next

# Time Complexity: O(n log k), where n is the total number of elements
# across all linked lists and k is the number of linked lists (lists).
# Space Complexity: O(k), where k is the size of the heap used to store
# the heads of the linked lists.
