# 79. Word Search
class Solution:
    def __init__(self):
        self.found = False

    def exist(self, board: list[list[str]], word: str) -> bool:
        for row in range(len(board)):
            for col in range(len(board[0])):
                if board[row][col] == word[0]:
                    self.search(board, row, col, word, count=0)
        return self.found

    def search(self, board, row, col, word, count):
        if count == len(word):
            self.found = True
            return
        if (row < 0 or row >= len(board) or col < 0 or col >= len(board[0])
                or board[row][col] != word[count] or self.found):
            return
        temp = board[row][col]
        board[row][col] = ""
        self.search(board, row + 1, col, word, count + 1)
        self.search(board, row - 1, col, word, count + 1)
        self.search(board, row, col + 1, word, count + 1)
        self.search(board, row, col - 1, word, count + 1)
        board[row][col] = temp
# Time complexity: O(N*M)
# Space complexity: O(len(word))

