# 54. Spiral Matrix
def spiralOrder(matrix: list[list[int]]) -> list[int]:
    res = []
    if not matrix:
        return res
    left = 0
    right = len(matrix[0]) - 1
    top = 0
    bottom = len(matrix) - 1
    direction = "right"
    while top <= bottom and left <= right:
        if direction == "right":
            for i in range(left, right + 1):
                res.append(matrix[top][i])
            top += 1
            direction = "down"
        elif direction == "down":
            for i in range(top, bottom + 1):
                res.append(matrix[i][right])
            right -= 1
            direction = "left"
        elif direction == "left":
            for i in range(right, left - 1, -1):
                res.append(matrix[bottom][i])
            bottom -= 1
            direction = "up"
        elif direction == "up":
            for i in range(bottom, top - 1, -1):
                res.append(matrix[i][left])
            left += 1
            direction = "right"
    return res
# Time complexity: O(m * n), where m is the number
# of rows and n is the number of columns in the matrix.
# Space complexity: O(m * n)