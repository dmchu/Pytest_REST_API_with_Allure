# 73. Set Matrix Zeroes
def setZeroes(matrix: list[list[int]]) -> None:
    first_row_has_zeros = False
    first_col_has_zeros = False

    for i in range(len(matrix[0])):
        if matrix[0][i] == 0:
            first_row_has_zeros = True

    for i in range(len(matrix)):
        if matrix[i][0] == 0:
            first_col_has_zeros = True

    for col in range(1, len(matrix[0])):
        for row in range(1, len(matrix)):
            if matrix[row][col] == 0:
                matrix[0][col] = 0
                matrix[row][0] = 0

    for col in range(1, len(matrix[0])):
        for row in range(1, len(matrix)):
            if matrix[0][col] == 0 or matrix[row][0] == 0:
                matrix[row][col] = 0

    if first_row_has_zeros:
        for i in range(len(matrix[0])):
            matrix[0][i] = 0

    if first_col_has_zeros:
        for i in range(len(matrix)):
            matrix[i][0] = 0
            
# Time complexity: O(N*M)
# Space complexity: O(1) (constant space)