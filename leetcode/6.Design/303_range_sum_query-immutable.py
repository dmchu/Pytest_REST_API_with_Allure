# 303. Range Sum Query - Immutable
class NumArray:

    def __init__(self, nums: list[int]):
        self.acc_sum = [0] * (len(nums) + 1)
        for i in range(len(nums)):
            self.acc_sum[i + 1] = self.acc_sum[i] + nums[i]

    def sumRange(self, left: int, right: int) -> int:
        return self.acc_sum[right + 1] - self.acc_sum[left]

    # Time complexity: O(1)
    # Space complexity: O(N)
    