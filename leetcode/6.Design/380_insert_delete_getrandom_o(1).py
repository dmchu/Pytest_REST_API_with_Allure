# 380. Insert Delete GetRandom O(1)
import random

class RandomizedSet:

    def __init__(self):
        self.nums = []
        self.map = {}

    def insert(self, val: int) -> bool:
        if val not in self.map:
            self.nums.append(val)
            self.map[val] = len(self.nums) - 1
            return True
        else:
            return False

    def remove(self, val: int) -> bool:
        if val not in self.map:
            return False
        else:
            idx_to_remove = self.map[val]
            last_num = self.nums[-1]
            self.map[last_num] = idx_to_remove
            self.nums[idx_to_remove], self.nums[-1] = self.nums[-1], self.nums[idx_to_remove]
            self.nums.pop()
            del self.map[val]
            return True

    def getRandom(self) -> int:
        return random.choice(self.nums)
# Time complexity: O(1)
# Space complexity: O(n) due to the storage required for the nums list and the map,
# where n is the number of elements in the set.
