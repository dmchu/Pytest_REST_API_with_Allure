# 384. Shuffle an Array
import random

class Solution:

    def __init__(self, nums: list[int]):
        self.array = nums
        self.original = nums.copy()

    def reset(self) -> list[int]:
        self.array = self.original.copy()
        return self.array

    def shuffle(self) -> list[int]:
        for i in range(len(self.array)):
            random_idx = random.randint(i, len(self.array) - 1)
            self.array[i], self.array[random_idx] = self.array[random_idx], self.array[i]
        return self.array

# Time complexity: O(N)
# Space complexity: O(N)
