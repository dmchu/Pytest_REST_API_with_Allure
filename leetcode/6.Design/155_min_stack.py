# 155. Min Stack
class MinStack:

    def __init__(self):
        self.stack = []
        self.min_stack = []

    def push(self, val: int) -> None:
        self.stack.append(val)
        previous_min = self.getMin()
        if previous_min != None and previous_min < val:
            self.min_stack.append(previous_min)
        else:
            self.min_stack.append(val)

    def pop(self) -> None:
        self.stack.pop()
        self.min_stack.pop()

    def top(self) -> int:
        return self.stack[-1]

    def getMin(self) -> int:
        if len(self.min_stack) > 0:
            return self.min_stack[-1]

# Time complexity: O(1)
# Space complexity: O(N)
