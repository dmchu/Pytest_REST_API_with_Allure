# 232. Implement Queue using Stacks
class MyQueue:

    def __init__(self):
        self.push_stack = []
        self.pop_stack = []

    def push(self, x: int) -> None:
        self.push_stack.append(x)

    def pop(self) -> int:
        self.peek()
        return self.pop_stack.pop()

    def peek(self) -> int:
        if not self.pop_stack:
            while self.push_stack:
                self.pop_stack.append(self.push_stack.pop())
            return self.pop_stack[-1]
        else:
            return self.pop_stack[-1]

    def empty(self) -> bool:
        return len(self.pop_stack) == 0 and len(self.push_stack) == 0

# Time complexity: O(1)
# Space complexity: O(n) due to the two stacks, where n
# is the number of elements in the queue
