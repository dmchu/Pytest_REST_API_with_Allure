# 70 Climbing Stairs
# Time comlexity: O(N) Our code loops N times
# Space comlexity: O(1) Array of size N is used

def climbStairs(self, n: int) -> int:
    if n == 1:
        return 1
    first = 1
    second = 2
    for i in range(3, n + 1):
        third = first + second
        first = second
        second = third

    return second
