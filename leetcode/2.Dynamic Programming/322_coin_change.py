# 322. Coin Change
# Time complexity: O(Amount*len(coins))
# Space complexity: O(Amount)  additional list dp of size amount + 1,
from typing import List

def coinChange(self, coins: List[int], amount: int) -> int:
    dp = [float('inf')] * (amount + 1)
    dp[0] = 0
    for i in range(1, len(dp)):
        amount = i
        for coin in coins:
            if coin <= amount:
                dp[i] = min(dp[amount - coin] + 1, dp[i])
    return dp[-1] if dp[-1] != float('inf') else -1
