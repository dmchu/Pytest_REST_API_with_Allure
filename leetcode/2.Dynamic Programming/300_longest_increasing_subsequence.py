# 300. Longest Increasing Subsequence
# Time complexity: O(N^2) We do up to N work, for all N elements
# Space complexity: O(N) We store the answer up to N sub problems.
from typing import List

def lengthOfLIS(self, nums: List[int]) -> int:
    if len(nums) == 0:
        return 0
    dp = [0] * len(nums)
    max_so_far = 1
    for i in range(len(nums)):
        for j in range(i):
            if nums[i] > nums[j]:
                dp[i] = max(dp[i], dp[j] + 1)
        max_so_far = max(max_so_far, dp[i] + 1)
    return max_so_far
