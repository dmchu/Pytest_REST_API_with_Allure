# 55. Jump Game
# Time complexity: O(N)
# Space complexity: O(1)
def canJump(self, nums: list[int]) -> bool:
    max_reach = 0
    for current_step in range(len(nums)):
        if current_step > max_reach:
            return False
        current_reach = current_step + nums[current_step]
        max_reach = max(max_reach, current_reach)

    return True
