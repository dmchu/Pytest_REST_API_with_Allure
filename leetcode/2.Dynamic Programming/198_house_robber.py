# 198. House Robber
# Time comlexity: O(N) Our code loops over input array once
# Space comlexity: O(N) Can be optimized to O(1) with two number variables
from typing import List

def rob(self, nums: List[int]) -> int:
    if len(nums) == 0:
        return 0
    if len(nums) == 1:
        return nums[0]
    if len(nums) == 2:
        return max(nums[0], nums[1])
    max_loot_at_nth = [nums[0], max(nums[0], nums[1])]

    for i in range(2, len(nums)):
        max_loot_at_nth.append(max(max_loot_at_nth[i - 1], max_loot_at_nth[i - 2] + nums[i]))
    return max_loot_at_nth[-1]
