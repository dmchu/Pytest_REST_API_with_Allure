# 62. Unique Paths
# Time complexity: O(N*M) because of nested loop
# Space complexity: O(N*M) dp matrix of size n*m
def uniquePaths(m: int, n: int) -> int:
    dp_matrix: list[list[int]] = [[1 for col in range(m)] for row in range(n)]
    for col in range(1, m):
        for row in range(1, n):
            dp_matrix[row][col] = (dp_matrix[row - 1][col] + dp_matrix[row][col - 1])
    return dp_matrix[-1][-1]
