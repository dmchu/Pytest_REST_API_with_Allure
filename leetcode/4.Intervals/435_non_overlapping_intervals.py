# 435. Non-overlapping Intervals
def eraseOverlapIntervals(intervals: list[list[int]]) -> int:
    if not intervals:
        return 0

    erase_count = 0

    intervals.sort(key=lambda interval: interval[0])

    prev_interval_end = intervals[0][1]

    for i in range(1, len(intervals)):
        if intervals[i][0] < prev_interval_end:
            erase_count += 1
            prev_interval_end = min(prev_interval_end, intervals[i][1])
        else:
            prev_interval_end = intervals[i][1]

    return erase_count

# Time complexity: O(n log n)
# Space complexity: O(1)
