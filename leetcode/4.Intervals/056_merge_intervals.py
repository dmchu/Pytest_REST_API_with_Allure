# 56. Merge Intervals
def merge(intervals: list[list[int]]) -> list[list[int]]:
    if not intervals:
        return []
    intervals.sort(key=lambda interval: interval[0])
    res = [intervals[0]]

    for i in range(1, len(intervals)):
        current_interval = intervals[i]
        prev_interval = res[-1]
        if current_interval[0] <= prev_interval[1]:
            prev_interval[1] = max(prev_interval[1], current_interval[1])
        else:
            res.append(current_interval)
    return res

# Time complexity: O(n log n)
# Space complexity: O(n) because the result list (res) can store up to n intervals.

