# LC 003 Longest Substring Without Repeating Characters
# Time comlexity: O(N)
# Space comlexity: O(min(m,n))
def lengthOfLongestSubstring(self, s: str) -> int:
    char_map = {}
    longest_s = 0
    window_start = 0
    for i in range(len(s)):
        last_char = s[i]
        if last_char and last_char in char_map and char_map[last_char] >= window_start:
            window_start = char_map[last_char] + 1
        char_map[last_char] = i
        longest_s = max(longest_s, i - window_start + 1)
    return longest_s
