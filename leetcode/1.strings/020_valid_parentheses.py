# 20 Valid Parentheses
# Time complexity: O(n) we iterate thru input string just once
# Space complexity: O(n) stack is same len as input string
def isValid(self, s: str) -> bool:
    if len(s) == 0:
        return False
    parentheses = {"(": ")", "{": "}", "[": "]"}
    stack = []
    for i in range(len(s)):
        if s[i] in parentheses.keys():
            stack.append(s[i])
        elif len(stack) == 0 or s[i] != parentheses[stack.pop()]:
            return False
    return len(stack) == 0
