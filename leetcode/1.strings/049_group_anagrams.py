# 49 Group Anagrams
# Time complexity: O(N * K log K) where N is # of strings,
# and K is len of strings
# Space complexity: O(N*K) Data stored in our grouped dict

def groupAnagrams(self, strs: list[str]) -> list[list[str]]:
    anagrams = {}
    for word in strs:
        key = "".join(sorted(word))
        if key not in anagrams:
            anagrams[key] = [word]
        else:
            anagrams[key].append(word)
    return anagrams.values()
