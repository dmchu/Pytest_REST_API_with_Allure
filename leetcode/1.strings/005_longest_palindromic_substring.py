# LC005 Longest Palindromic Substring
# Time comlexity: O(N^2)
# Space comlexity: O(1)
def is_palindrome(self, word, left, right):
    while left >= 0 and right < len(word) and word[left] == word[right]:
        left -= 1
        right += 1
    return word[left + 1: right]

def longestPalindrome(self, s: str) -> str:
    longest_palindrome = ""
    for i in range(len(s)):
        current = self.is_palindrome(s, i - 1, i + 1)
        in_the_middle = self.is_palindrome(s, i, i + 1)
        longest_palindrome = max(longest_palindrome, current, in_the_middle, key=len)
    return longest_palindrome
