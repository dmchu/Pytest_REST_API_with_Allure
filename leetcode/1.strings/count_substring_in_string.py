input_string = "aabbbccaaabbbbbc"
def count_substring(string):
    word_list = list(string)
    result = ""
    count = 1
    word_list.append("")
    for i in range(len(word_list)-1):
        if word_list[i] == word_list[i + 1]:
            count += 1
        else:
            result += f"{str(count)}{word_list[i]}"
            count = 1
    return result

print(count_substring(input_string))

