
# 125.Valid Palindrome
# Time comlexity: O(N)
# Space comlexity: O(1)
import re

def isPalindrome(self, s: str) -> bool:
    s = re.sub(r'[\W_]', '', s).lower()
    left = 0
    right = len(s) - 1
    while left < right:
        if s[left] != s[right]:
            return False
        left += 1
        right -= 1
    return True