word = "Mannhattan"

def reverse_word(word: str) -> str:
    list_string = list(word)
    start = 0
    end = len(list_string) - 1
    while start <= end:
        list_string[start], list_string[end] = list_string[end], list_string[start]
        start += 1
        end -= 1
    return "".join(list_string)

print(reverse_word(word))


