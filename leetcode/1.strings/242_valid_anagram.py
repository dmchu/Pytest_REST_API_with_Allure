# 242 Valid Anagram
# Time comlexity: O(n)
# Space comlexity: O(1)
def isAnagram(self, s: str, t: str) -> bool:
    if len(s) != len(t):
        return False
    letters_s = {}
    for i in range(len(s)):
        letter = s[i]
        if letter in letters_s:
            letters_s[letter] += 1
        else:
            letters_s[letter] = 1

    for i in range(len(t)):
        char = t[i]
        if char not in letters_s or letters_s[char] == 0:
            return False
        else:
            letters_s[char] -= 1

    return True
