# 844 Backspace String Compare
# Time complexity: O(n + m) n and m are len of input strings
# Space complexity: O(n + m) from our stacks
def backspaceCompare(self, s: str, t: str) -> bool:
    s_stack = []
    t_stack = []

    for char in s:
        if char != "#":
            s_stack.append(char)
        elif char == "#" and len(s_stack) > 0:
            s_stack.pop()

    for char in t:
        if char != "#":
            t_stack.append(char)
        elif char == "#" and len(t_stack) > 0:
            t_stack.pop()

    return s_stack == t_stack
