# 206. Reverse Linked List
def reverseList(head: Optional[ListNode]) -> Optional[ListNode]:
    prev = None
    current = head
    while current:
        temp = current.next
        current.next = prev
        prev = current
        current = temp

    return prev

# Time complexity: O(N)
# Space complexity: O(1)
