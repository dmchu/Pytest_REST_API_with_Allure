# 203. Remove Linked List Elements
def removeElements(head: Optional[ListNode], val: int) -> Optional[ListNode]:
    dummy = ListNode(None)
    dummy.next = head
    current = dummy
    while current:
        if current.next and current.next.val == val:
            current.next = current.next.next
        else:
            current = current.next
    return dummy.next
# Time complexity: O(N)
# Space complexity: O(1)
