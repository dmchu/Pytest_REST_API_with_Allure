# 141. Linked List Cycle
def hasCycle(head: Optional[ListNode]) -> bool:
    slow = head
    fast = head

    while fast and fast.next:
        slow = slow.next
        fast = fast.next.next
        if slow == fast:
            return True
    return False
# Time complexity: O(N)
# Space complexity: O(1)
# It efficiently detects cycles in a linked list using
# the “tortoise and hare” approach
