# 19. Remove Nth Node From End of List
def removeNthFromEnd(head: Optional[ListNode], n: int) -> Optional[ListNode]:
    dummy = ListNode(None)
    dummy.next = head

    slow = dummy
    fast = dummy
    for _ in range(n):
        fast = fast.next
    while fast and fast.next:
        slow = slow.next
        fast = fast.next
    slow.next = slow.next.next
    return dummy.next
# Time complexity: O(N)
# Space complexity: O(1)
