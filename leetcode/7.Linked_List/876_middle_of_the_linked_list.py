# 876. Middle of the Linked List
def middleNode(head: Optional[ListNode]) -> Optional[ListNode]:
    slow = head
    fast = head
    while fast and fast.next:
        slow = slow.next
        fast = fast.next.next
    return slow
# Time complexity: O(N)
# Space complexity: O(1)
