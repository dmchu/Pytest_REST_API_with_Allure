# 143. Reorder List
class Solution:
    def reverse_list(self, node):
        prev = None
        current = node
        while current:
            temp = current.next
            current.next = prev
            prev = current
            current = temp
        return prev

    def reorderList(self, head: Optional[ListNode]) -> None:
        # Step 1: Split ll into 2 equal parts
        slow = head
        fast = head
        while fast.next and fast.next.next:
            slow = slow.next
            fast = fast.next.next
        l2 = self.reverse_list(slow.next)
        slow.next = None
        # Zipper 2 lists togather
        current = head
        current2 = l2
        while current2:
            temp1 = current.next
            temp2 = current2.next
            current.next = current2
            current2.next = temp1
            current = temp1
            current2 = temp2
# Time complexity: O(N)
# Space complexity: O(1)
