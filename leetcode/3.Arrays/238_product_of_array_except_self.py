# 238. Product of Array Except Self
# Time complexity: O(n) Both loops together take O(n) time
# (since each loop iterates through the entire nums list).
# Space complexity: O(n) the code uses an additional list dp of size n
def productExceptSelf(nums: list[int]) -> list[int]:
    cumulative = 1
    dp = [1] * len(nums)
    # From the left:
    for i in range(len(nums)):
        dp[i] = cumulative * dp[i]
        cumulative = cumulative * nums[i]
    # From the right
    cumulative = 1
    for i in range(len(nums) - 1, -1, -1):
        dp[i] = cumulative * dp[i]
        cumulative = cumulative * nums[i]
    return dp
