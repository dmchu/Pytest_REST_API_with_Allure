# 11. Container With Most Water

def maxArea(height: list[int]) -> int:
    max_s = 0
    left = 0
    right = len(height) - 1
    while left < right:
        s = (right - left) * min(height[left], height[right])
        max_s = max(max_s, s)
        if height[left] <= height[right]:
            left += 1
        else:
            right -= 1
    return max_s

# Time complexity: O(n) Since the pointers move towards each other,
# the function processes each element in the height array exactly once.
# Therefore, the time complexity is linear, O(n).
# Space complexity: O(1) the function uses only a few variables
# (max_s, left, and right) without any additional data structures
# that depend on the input size.