# 152. Maximum Product Subarray
def maxProduct(nums: list[int]) -> int:
    max_dp = [nums[0]]
    min_dp = [nums[0]]
    max_product = nums[0]

    for i in range(1, len(nums)):
        prev_max = max_dp[i - 1]
        prev_min = min_dp[i - 1]

        max_dp.append(max(nums[i], prev_max * nums[i], prev_min * nums[i]))
        min_dp.append(min(nums[i], prev_max * nums[i], prev_min * nums[i]))

        max_product = max(max_product, max_dp[i])

    return max_product

# Time complexity: O(N)
# Space complexity: O(N)
