# 121. Best Time to Buy and Sell Stock
def maxProfit(prices: list[int]) -> int:
    min_price = prices[0]
    max_profit = 0
    for price in prices:
        min_price = min(min_price, price)
        current_profit = price - min_price
        max_profit = max(max_profit, current_profit)

    return max_profit

# Time complexity: O(n)
# Space complexity: O(1)