# 128. Longest Consecutive Sequence
def longestConsecutive(nums: list[int]) -> int:
    longest_steak: int = 0
    nums_set: set[int] = set(nums)
    for num in nums:
        if num - 1 not in nums_set:
            current_num = num
            current_steak = 1
            while current_num + 1 in nums_set:
                current_num += 1
                current_steak += 1
            longest_steak = max(longest_steak, current_steak)
    return longest_steak

# Time complexity: O(N)
# Space complexity: O(N)
