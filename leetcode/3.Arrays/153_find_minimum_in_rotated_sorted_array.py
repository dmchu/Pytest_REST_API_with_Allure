# 153. Find Minimum in Rotated Sorted Array
def findMin(nums: list[int]) -> int:
    if len(nums) == 1:
        return nums[0]

    left, right = 0, len(nums) - 1

    if nums[left] < nums[right]:
        return nums[left]

    while left < right:
        middle = (left + right) // 2
        if nums[middle] > nums[right]:
            left = middle + 1
        else:
            right = middle

    return nums[left]

# Time complexity: O(log N) where N is the length of the input list nums
# Space complexity: O(1)
