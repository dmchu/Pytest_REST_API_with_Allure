# 217. Contains Duplicate
# Time complexity: O(n) loop takes O(n)
# Space complexity: O(n) maximum size of nums_dict
# is equal to the number of unique elements in nums
def containsDuplicate(nums: list[int]) -> bool:
    nums_dict = {}
    for num in nums:
        if nums_dict.get(num):
            return True
        else:
            nums_dict[num] = 1
    return False
