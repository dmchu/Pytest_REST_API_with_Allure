# 1. Two Sum
def twoSum(nums: list[int], target: int) -> list[int]:
    nums_visited = {}
    for i in range(len(nums)):
        num = nums[i]
        add_num = target - num
        if add_num in nums_visited:
            return [i, nums_visited[add_num]]
        else:
            nums_visited[num] = i

# Time complexity: O(n) where n is the length of the nums array
# Space complexity: O(n) In the worst case, the dictionary may
# store all n elements from the nums array.
