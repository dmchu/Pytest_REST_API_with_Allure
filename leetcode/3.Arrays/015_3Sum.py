# 15. 3Sum
def threeSum(nums: list[int]) -> list[list[int]]:
    result: list[list[int]] = []
    nums.sort()
    for i in range(len(nums) - 2):
        if i > 0 and nums[i] == nums[i - 1]:
            continue
        add_number = 0 - nums[i]
        left = i + 1
        right = len(nums) - 1
        while left < right:
            if nums[left] + nums[right] == add_number:
                result.append([nums[left], nums[right], nums[i]])
                while left < right and nums[left] == nums[left + 1]:
                    left += 1
                left += 1
                right -= 1
            elif nums[left] + nums[right] < add_number:
                left += 1
            else:
                right -= 1
    return result

# Time complexity: O(N^2) for each number we check all numbers ahead of it.
# Space complexity: O(1)
