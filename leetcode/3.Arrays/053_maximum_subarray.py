# 53. Maximum Subarray
def maxSubArray(nums: list[int]) -> int:
    currMax = globalMax = nums[0]
    for i in range(1, len(nums)):
        currMax = max(nums[i], currMax + nums[i])
        globalMax = max(globalMax, currMax)
    return globalMax

# Time complexity: O(N)
# Space complexity: O(1)
